/// <reference path="../lib/d.ts/TypeScript/dom.generated.d.ts">

'use strict';

console.log('\'Allo \'Allo!');

var myFile;

(function (myFile) {
  var init = (function () {
    function init() {
      var dropArea = document.getElementById('dropArea');
      dropArea.addEventListener('dragover', dragOver, true);
      dropArea.addEventListener('drop', drop, true);
    }

    return init;
  })();

  function dragOver(event) {
    event.preventDefault();
  }

  /**
   *
   * @param {DragEvent} event
   */
  function drop(event) {
    event.preventDefault();
    var dataTransfer = event.dataTransfer;
    var files = dataTransfer.files;

    for (var i = 0; i < files.length; i++) {
      console.log(files[i].name);
      readFile(files[i]);
    }
  }


  /**
   *
   * @param {File} file
   */
  function readFile(file) {
    var reader = new FileReader();

    /**
     *
     * @param {ProgressEvent} event
     */
    reader.onabort = function (event) {
      console.log('* Abort');
      //console.log(event);
    };

    /**
     *
     * @param {ProgressEvent} event
     */
    reader.onerror = function (event) {
      console.log('* Error');
      //console.log(event);
    };

    /**
     *
     * @param {ProgressEvent} event
     */
    reader.onload = function (event) {
      console.log('* Load');
      //console.log(event);

      var loaded = event.loaded;
      var dataView = new DataView(event.target.result);

      dumpInfo(dataView, 0, loaded, 0);
    };

    /**
     *
     * @param {ProgressEvent} event
     */
    reader.onloadstart = function (event) {
      console.log('* Load Start: ' + event.loaded + ' / ' + event.total);
      //console.log(event);

      document.querySelector('progress').value = event.loaded / event.total * 100;
    };

    /**
     *
     * @param {ProgressEvent} event
     */
    reader.onloadend = function (event) {
      console.log('* Load End: ' + event.loaded + ' / ' + event.total);
      //console.log(event);
    };

    /**
     *
     * @param {ProgressEvent} event
     */
    reader.onprogress = function (event) {
      //console.log('* Progress: ' + event.loaded + ' / ' + event.total);
      //console.log(event);

      document.querySelector('progress').value = event.loaded / event.total * 100;
    };

    /**
     *
     * @param {DataView} dataView
     * @param p
     * @param last
     * @param indent
     */
    function dumpInfo(dataView, p, last, indent) {
      //console.log("indent: " + indent);
      var a = '';
      for (var i = 0; i < indent; i++) {
        a += ' ';
      }
      while (p < last) {
        //console.log("p: " + p);
        var length = dataView.getUint32(p);
        var type = String.fromCharCode(dataView.getUint8(p + 4), dataView.getUint8(p + 5), dataView.getUint8(p + 6), dataView.getUint8(p + 7));
        switch (type) {
          case 'moov':  // movie header
          case 'trak':  // track header
          case 'mdia':  // media
          case 'minf':  // media information
          case 'dinf':
          case 'stbl':  // sample table
          case 'udta':  // user data atom
          case 'edts':  // edit atom
            console.log(a + type + ': ' + length);
            //dumpInfo(dataView, p + 8, last - 8, indent + 1);
            dumpInfo(dataView, p + 8, p + length, indent + 1);
            break;
          case 'ftyp':  // file type
            console.log(a + type + ': ' + length);
            dumpFtyp(dataView, p, length);
            break;
          case 'tkhd':  // track header
            console.log(a + type + ': ' + length);
            break;
          case 'mdat':  // media data
          case 'mvhd':  // movie header
          case 'vmhd':  // video media header
          case 'smhd':  // sound media header
          case 'mdhd':  // media header
          case 'elst':  // edit list
          case 'dref':
          case 'hdlr':  // handler type
          case 'stsd':  // sample table sample description
          case 'stts':  // sample table time to sample map
          case 'stss':  // sample table sync samples
          case 'stsc':  // sample table sample to chunk map
          case 'stsz':  // sample table sizes
          case 'stco':  // sample table chunk offset
          case 'meta':
          case 'ctts':  // 32 bits difference PTS-DTS
          case 'iods':
          case 'name':
          case 'free':  // free space
            console.log(a + type + ': ' + length);
            break;
          default:
            console.log(a + type + ': ' + length);
            //dumpInfo(dataView, p + 8, last - 8, indent + 1);
            dumpInfo(dataView, p + 8, p + length, indent + 1);
            break;
        }
        p = p + length;
      }
    }

    /**
     *
     * @param {DataView} dataView
     * @param p
     * @param length
     */
    function dumpFtyp(dataView, p, length) {
      var last = p + length;
      p += 8;
      var majorBland = String.fromCharCode(dataView.getUint8(p++), dataView.getUint8(p++), dataView.getUint8(p++), dataView.getUint8(p++));
      var version = dataView.getUint32(p);
      p += 4;
      console.log('  * ' + 'Major Bland: ' + majorBland);
      console.log('  * ' + 'Version: 0x' + version.toString(16));

      var compatibleBland = [];
      while (p < last) {
        compatibleBland.push(String.fromCharCode(dataView.getUint8(p++), dataView.getUint8(p++), dataView.getUint8(p++), dataView.getUint8(p++)));
      }
      console.log('  * ' + 'Compatible Bland: ' + compatibleBland);
    }

    reader.readAsArrayBuffer(file);
  }

  myFile.init = init;
})(myFile || (myFile = {}));

window.addEventListener('load', myFile.init);
